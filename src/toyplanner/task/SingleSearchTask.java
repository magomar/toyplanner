package toyplanner.task;

import toyplanner.search.algorithms.Search;
import toyplanner.search.data.Path;
import toyplanner.search.data.State;

/**
 * Created by mario on 31/01/14.
 */
public class SingleSearchTask<S extends State> implements Task {
    private S initialState;
    private S goalState;
    private Path<S> solution;
    private Search algorithm;

    public SingleSearchTask(S initialState, S goalState, Search algorithm) {
        this.initialState = initialState;
        this.goalState = goalState;
        this.algorithm = algorithm;
    }

    @Override
    public void run() {
        System.out.println("\nExecuting Single Search Task");
        System.out.println("==============================");
        System.out.println("  Initial state: " + initialState);
        System.out.println("  Goal state: " + goalState);
        System.out.println("  Algorithm: " + algorithm);
        solution = algorithm.search(initialState, goalState);
        if (solution != null) {
            System.out.println("  Solution found: " + solution);
        } else {
            System.out.println("  No solution found !");
        }
        System.out.println("  Nodes explored/generated: " + algorithm.getNodesExplored()+"/"+algorithm.getNodesGenerated());
    }

}
