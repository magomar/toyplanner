package toyplanner.search.data;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mario on 12/02/14.
 */
public class GNode<S extends State> extends  AbstractNode<S> {
    protected double gValue;


    public GNode(S state, int depth, Action action, GNode<S> antecedent, double gValue) {
        super(state, depth, action, antecedent);
        this.gValue = gValue;
    }

    public Set<GNode<S>> getSuccessors() {
        Set<GNode<S>> successors = new HashSet<>();
        Set<Action> applicableActions = state.getApplicableActions();
        int successorDepth = depth + 1;
        for (Action applicableAction : applicableActions) {
            S successorState = (S) state.apply(applicableAction);
            successors.add(new GNode<>(successorState, successorDepth, applicableAction,this, successorDepth));
        }
        return successors;
    }

    @Override
    public int compareTo(Node<S> node) {
        double gDiff = gValue - ((GNode) node).gValue;
        if (gDiff > 0) return 1;
        if (gDiff < 0) return -1;
        return 0;
    }

    public String toStringVerbose() {
        StringBuilder sb = new StringBuilder();
        sb.append(state).append(" G: ");
        sb.append(gValue).append(", Action: ");
        sb.append(action).append(" @ ");
        sb.append(depth);
        return sb.toString();
    }
}
