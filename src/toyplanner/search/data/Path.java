package toyplanner.search.data;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Mario on 12/02/14.
 */
public class Path<S extends State> {
    Deque<Node<S>> path;

    public Path(Node<S> lastNode) {
        path = new LinkedList<>();
        Deque<Node<S>> reversePath = new LinkedList<>();
        reversePath.add(lastNode);
        Node<S> node = lastNode.getAntecedent();
        while (node != null) {
            reversePath.add(node);
            node = node.getAntecedent();
        }
        Iterator<Node<S>> reverseIterator = reversePath.descendingIterator();
        while (reverseIterator.hasNext()) {
            path.add(reverseIterator.next());
        }
    }

    public Deque<Node<S>> getPath() {
        return path;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Solution lenght: ");
        sb.append(path.size()).append("\nPath: ");
        sb.append(path);
        return sb.toString();
    }
}
