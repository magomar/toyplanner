package toyplanner.search.data;

import java.util.Set;

/**
 * Created by mario on 30/01/14.
 */
public interface State {

    Set<State> getSuccessors(Set<Action> actions);
    State apply(Action action);
    Set<Action> getApplicableActions();
    boolean equals(Object obj);
    int hashCode();
}
