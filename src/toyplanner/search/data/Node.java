package toyplanner.search.data;

/**
 * Created by mario on 12/02/14.
 */
public interface Node<S extends State> extends Comparable<Node<S>> {
    int getDepth();
    S getState();
    Action getAction();
    Node<S> getAntecedent();
}
