package toyplanner.search.data;

import toyplanner.search.algorithms.Heuristic;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mario on 30/01/14.
 */
public class HNode<S extends State> extends GNode<S> {
    private double hValue;
    private double fValue;

    public HNode(S state, int depth, Action action, HNode<S> antecedent, Heuristic<S> heuristic, S goalState) {
        super(state, depth, action, antecedent, depth);
        this.action = action;
        this.hValue = heuristic.getHeuristicValue(state, goalState);
        fValue = gValue + hValue;
    }

    public double getFValue() {
        return fValue;
    }

    public Set<HNode<S>> getSuccessors(Heuristic<S> heuristic, S goalState) {
        Set<HNode<S>> successors = new HashSet<>();
        Set<Action> applicableActions = state.getApplicableActions();
        int successorDepth = depth + 1;
        for (Action applicableAction : applicableActions) {
            S successorState = (S) state.apply(applicableAction);
            successors.add(new HNode<>(successorState, successorDepth, applicableAction, this, heuristic, goalState));
        }
        return successors;
    }

    @Override
    public int compareTo(Node<S> node) {
        HNode<S> hNode = (HNode<S>) node;
        double fDiff = fValue - hNode.fValue;
        if (fDiff > 0) return 1;
        if (fDiff < 0) return -1;
        // diff == 0
        double gDiff = gValue - hNode.gValue;
        if (gDiff > 0) return 1;
        if (gDiff < 0) return -1;
        return 0;
    }

    public String toStringVerbose() {
        StringBuilder sb = new StringBuilder();
        sb.append(state).append(" F: ");
        sb.append(fValue).append(", Action: ");
        sb.append(action).append(" @ ");
        sb.append(depth);
        return sb.toString();
    }
}
