package toyplanner.search.data;

/**
 * Created by mario on 12/02/14.
 */
public abstract class AbstractNode<S extends State> implements Node<S> {
    protected int depth;
    protected S state;
    protected Action action;
    protected Node<S> antecedent;

    protected AbstractNode(S state, int depth, Action action, Node<S> antecedent) {
        this.depth = depth;
        this.state = state;
        this.action = action;
        this.antecedent = antecedent;
    }

    @Override
    public int getDepth() {
        return depth;
    }

    @Override
    public S getState() {
        return state;
    }

    @Override
    public Action getAction() {
        return action;
    }

    @Override
    public Node<S> getAntecedent() {
        return antecedent;
    }

    @Override
    public int hashCode() {
        return state.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return state.equals(((Node<S>) obj).getState());
    }

    @Override
    public String toString() {
        return action + "=>" + state.toString();
    }
}
