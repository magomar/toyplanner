package toyplanner.search.data;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mario on 12/02/14.
 */
public abstract class AbstractState implements State {

    @Override
    public Set<State> getSuccessors(Set<Action> actions) {
        Set<Action> applicableActions = getApplicableActions();
        Set<State> successors = new HashSet<>();
        for (Action applicableAction : applicableActions) {
            successors.add(apply(applicableAction));
        }
        return successors;
    }
}
