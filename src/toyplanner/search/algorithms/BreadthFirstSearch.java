package toyplanner.search.algorithms;

import toyplanner.search.data.GNode;
import toyplanner.search.data.Path;
import toyplanner.search.data.State;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Created by mario on 11/02/14.
 */
public class BreadthFirstSearch<S extends State> implements Search<S> {
    protected Queue<GNode<S>> fringe;
    protected int nodesExplored;
    protected Set<GNode<S>> visited;

    @Override
    public Path<S> search(S initialState, S goalState) {
        nodesExplored = 0;
        fringe = new LinkedList<>();
        visited = new HashSet<>();

        GNode<S> initialNode = new GNode<>(initialState, 0, null, null, 0);
        fringe.add(initialNode);
        while (!fringe.isEmpty()) {
            GNode<S> currentNode = fringe.poll();
            nodesExplored++;
            // Check if goal is reached
            if (currentNode.getState().equals(goalState)) {
                return new Path<>(currentNode);
            }
            Set<GNode<S>> successors = currentNode.getSuccessors();
            for (GNode<S> successor : successors) {
                if (!visited.contains(successor)) {
                    fringe.add(successor);
                    visited.add(successor);
                }
            }
        }
        return null;
    }

    @Override
    public int getNodesExplored() {
        return nodesExplored;
    }

    @Override
    public int getNodesGenerated() {
        return visited.size();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
