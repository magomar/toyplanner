package toyplanner.search.algorithms;

import toyplanner.search.data.HNode;
import toyplanner.search.data.Node;
import toyplanner.search.data.Path;
import toyplanner.search.data.State;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * Created by Mario on 12/02/14.
 * Implements Best-First Search for graphs
 */
public class BestFirstSearch<S extends State> implements HeuristicSearch<S> {
    protected PriorityQueue<HNode<S>> fringe;
    protected int nodesExplored;
    protected Set<HNode<S>> visited;
    private Heuristic<S> heuristic;

    public BestFirstSearch(Heuristic<S> heuristic) {
        this.heuristic = heuristic;
    }

    @Override
    public void setHeuristic(Heuristic<S> heuristic) {
        this.heuristic = heuristic;
    }

    @Override
    public Heuristic<S> getHeuristic() {
        return heuristic;
    }

    @Override
    public Path<S> search(S initialState, S goalState) {
        nodesExplored = 0;
        fringe = new PriorityQueue<>();
        visited = new HashSet<>();

        HNode<S> initialNode = new HNode(initialState, 0, null, null, heuristic, goalState);
        fringe.add(initialNode);
        while (!fringe.isEmpty()) {
            HNode<S> currentNode = fringe.poll();
            nodesExplored++;
            if (currentNode.getState().equals(goalState)) {
                return new Path<>(currentNode);
            }
            Set<HNode<S>> successors = currentNode.getSuccessors(heuristic, goalState);
            for (HNode<S> successor : successors) {
                if (!visited.contains(successor)) {
                    fringe.add(successor);
                    visited.add(successor);
                }
            }
        }
        return null;
    }

    @Override
    public int getNodesExplored() {
        return nodesExplored;
    }

    @Override
    public int getNodesGenerated() {
        return visited.size();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " with heuristic " + heuristic;
    }
}
