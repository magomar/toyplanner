package toyplanner.search.algorithms;

import toyplanner.search.data.State;

/**
 * Created by mario on 31/01/14.
 */
public interface HeuristicSearch<S extends State> extends Search<S> {
    void setHeuristic(Heuristic<S> heuristic);

    Heuristic<S> getHeuristic();
}
