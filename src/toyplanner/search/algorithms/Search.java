package toyplanner.search.algorithms;

import toyplanner.search.data.Path;
import toyplanner.search.data.State;

/**
 * Created by mario on 30/01/14.
 */
public interface Search<S extends State> {

    Path<S> search(S initialState, S goalState);

    int getNodesExplored();

    int getNodesGenerated();
}
