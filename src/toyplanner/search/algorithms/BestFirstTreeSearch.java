package toyplanner.search.algorithms;

import toyplanner.search.data.HNode;
import toyplanner.search.data.Path;
import toyplanner.search.data.State;

import java.util.PriorityQueue;
import java.util.Set;

/**
 * Created by mario on 30/01/14.
 * Implements Best-First Search for trees
 * <p/>
 * General Tree Search Algorithm
 * function treeSearch(problem, strategy)
 * fringe = { new searchNode(problem.initialState) }
 * loop
 * if empty(fringe) then return failure
 * node = selectFrom(fringe, strategy)
 * if problem.goalTest(node.state) then return pathTo(node)
 * fringe = fringe + expand(problem, node)
 */
public class BestFirstTreeSearch<S extends State> implements HeuristicSearch<S> {
    protected PriorityQueue<HNode<S>> fringe;
    protected Heuristic<S> heuristic;
    protected int nodesExplored;
    protected int nodesGenerated;

    public BestFirstTreeSearch(Heuristic<S> heuristic) {
        this.heuristic = heuristic;

    }

    @Override
    public Path<S> search(S initialState, S goalState) {
        nodesExplored = 0;
        fringe = new PriorityQueue<>();
        HNode<S> initialNode = new HNode<>(initialState, 0, null, null, heuristic, goalState);
        fringe.add(initialNode);
        while (!fringe.isEmpty()) {
            HNode<S> currentNode = fringe.poll();
            nodesExplored++;
            if (currentNode.getState().equals(goalState)) {
                return new Path<>(currentNode);
            }
            Set<HNode<S>> successors = currentNode.getSuccessors(heuristic, goalState);
            fringe.addAll(successors);
            nodesGenerated += successors.size();
        }
        return null;
    }

    @Override
    public int getNodesExplored() {
        return nodesExplored;
    }

    @Override
    public int getNodesGenerated() {
        return nodesGenerated;
    }

    @Override
    public void setHeuristic(Heuristic<S> heuristic) {
        this.heuristic = heuristic;
    }

    @Override
    public Heuristic<S> getHeuristic() {
        return heuristic;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " with heuristic " + heuristic;
    }

}
