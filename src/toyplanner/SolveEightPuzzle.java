package toyplanner;

import toyplanner.eightpuzzle.EightPuzzleHeuristic;
import toyplanner.eightpuzzle.EightPuzzleState;
import toyplanner.search.algorithms.BestFirstSearch;
import toyplanner.search.algorithms.BreadthFirstSearch;
import toyplanner.search.algorithms.Search;
import toyplanner.task.SingleSearchTask;
import toyplanner.task.TaskRunner;

/**
 * Created by mario on 12/02/14.
 */
public class SolveEightPuzzle {
    public static void main(String[] args) {
        final int[][] goalPuzzle = new int[][]{{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};
        final int maxDepth = 27;
        final int[][] initialPuzzle = new int[][]{{3, 1, 5}, {6, 4, 0}, {7, 2, 8}}; // VERY EASY (Cost = 11)
//        final int[][] initialPuzzle = new int[][]{{3, 1, 5},{ 6, 4, 7},{0, 2, 8}}; // EASY (Cost = 18)
//        final int[][] initialPuzzle = new int[][]{{3, 1, 5},{ 6, 4, 0},{7, 8, 2}}; // HAS NO SOLUTION
//        final int[][] initialPuzzle = new int[][]{{1, 6, 4},{ 8, 7, 0},{3, 2, 5}}; // Question 1
//        final int[][] initialPuzzle = new int[][]{{8, 1, 7},{4, 5, 6},{2, 0, 3}}; // Question 2
        EightPuzzleState initialState= new EightPuzzleState(initialPuzzle);
        EightPuzzleState goalState = new EightPuzzleState(goalPuzzle);

        // Breadth-First
        Search search = new BreadthFirstSearch();
        SingleSearchTask<EightPuzzleState> searchTask = new SingleSearchTask<>(initialState, goalState, search);
        TaskRunner.getInstance().run(searchTask);

        // Best-First
        // Hammings distance
        search = new BestFirstSearch(EightPuzzleHeuristic.HAMMINGS_DISTANCE);
        searchTask = new SingleSearchTask<>(initialState, goalState, search);
        TaskRunner.getInstance().run(searchTask);
        // Manhattan distance
        search = new BestFirstSearch(EightPuzzleHeuristic.MANHATTAN_DISTANCE);
        searchTask = new SingleSearchTask<>(initialState, goalState, search);
        TaskRunner.getInstance().run(searchTask);
    }
}
