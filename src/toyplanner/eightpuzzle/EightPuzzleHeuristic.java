package toyplanner.eightpuzzle;


import toyplanner.search.algorithms.Heuristic;

/**
 * Created by mario on 30/01/14.
 */
public enum EightPuzzleHeuristic implements Heuristic<EightPuzzleState> {
    HAMMINGS_DISTANCE {
        @Override
        public double getHeuristicValue(EightPuzzleState currentState, EightPuzzleState goalState) {
            int[][] puzzle = currentState.getPuzzle();
            int[][] goalPuzzle = goalState.getPuzzle();
            int distance = 0;
            for (int i = 0; i < 3; i++) {
                int[] puzzleRow = puzzle[i];
                int[] goalRow = goalPuzzle[i];
                for (int j = 0; j < 3; j++) {
                    if (puzzleRow[j] != goalRow[j]) distance++;
                }
            }
            return distance;
        }
    },
    MANHATTAN_DISTANCE {
        @Override
        public double getHeuristicValue(EightPuzzleState currentState, EightPuzzleState goalState) {
            int[][] puzzle = currentState.getPuzzle();
//            int[][] goalPuzzle = goalState.getPuzzle();
            ManhattanDistanceHelper manhattanDistanceHelper = ManhattanDistanceHelper.getManhattanDistanceHelper(goalState);
            int[] targetX = manhattanDistanceHelper.getX();
            int[] targetY = manhattanDistanceHelper.getY();
            int distance = 0;
            for (int i = 0; i < 3; i++) {
                int[] puzzleRow = puzzle[i];
                for (int j = 0; j < 3; j++) {
                    int value = puzzleRow[j];
                    if (value != 0) {
                        int dx = i - targetX[value];
                        int dy = j - targetY[value];
                        distance += Math.abs(dx) + Math.abs(dy);
                    }
                }
            }
            return distance;
        }
    };

}
