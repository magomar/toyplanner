package toyplanner.eightpuzzle;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mario on 1/02/14.
 */
public class ManhattanDistanceHelper {
    private int[] x = new int[9];
    private int[] y = new int[9];
    private static Map<EightPuzzleState, ManhattanDistanceHelper> cache = new HashMap<>();

    public static ManhattanDistanceHelper getManhattanDistanceHelper(EightPuzzleState goalState) {
        if (cache.containsKey(goalState)) return cache.get(goalState);
        else {
            ManhattanDistanceHelper manhattanDistanceHelper = new ManhattanDistanceHelper(goalState);
            cache.put(goalState, manhattanDistanceHelper);
            return manhattanDistanceHelper;
        }
    }


    private ManhattanDistanceHelper(EightPuzzleState goalState) {
        int[][] puzzle = goalState.getPuzzle();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int value = puzzle[i][j];
                x[value] = i;
                y[value] = j;
            }
        }
    }

    public int[] getX() {
        return x;
    }

    public int[] getY() {
        return y;
    }
}
